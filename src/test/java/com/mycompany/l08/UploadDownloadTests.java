package com.mycompany.l08;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;


public class UploadDownloadTests extends TestBase {

    @Test
    public void uploadFile() {
        driver.get("https://yandex.ru/images/");
        driver.findElement(By.xpath("//button[@aria-label='Поиск по изображению']")).click();
        var photoIcon = driver.findElement(By.xpath("//input[@class='cbir-panel__file-input']"));
        var photo = new File("src/test/resources/test_automation.png");
        photoIcon.sendKeys(photo.getAbsolutePath());
        Assert.assertTrue(driver.findElement(By.xpath("//h2[contains(text(),'Сайты с информацией про изображение')]")).isDisplayed());
    }

    @Test
    public void downloadFile()  {
        driver.get("https://ru.vividscreen.info/");
        driver.findElement(By.cssSelector("img[src$='/Mercedes-Benz-C-Class-Estate-AMG-Line-2021-wide-l.jpg']")).click();
        driver.findElement(By.linkText("Скачать картинку")).click();

        File f = new File("src/test/resources/Mercedes-Benz-C-Class-Estate-AMG-Line-2021-wide.jpg");
        if(f.exists()) {
            System.out.println("File was successfully downloaded");
            }
        else {
            System.out.println("File wasn't downloaded");
        }

    }
}
